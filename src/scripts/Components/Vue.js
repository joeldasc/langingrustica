//const imgLink = '../../public/images/';
const imgLink = '/landing/public/images/';

window.axios = require('axios');
let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

import postscribe from 'postscribe';

const app = new Vue({
    el: "#app-rustica",
    data() {
        return {
            isActive: false,
            is_show_continue: true,
            action_pay_post: null,
            is_resent_load: true,
            session_payment: null,
            client: {
                dni: null,
                email: null,
                name: null,
                lastname: null,
            },
            listCard: [
                {
                    id: 4,
                    price: 30,
                    priceOffert: 33,
                    image: imgLink + 'card33.png',
                    imageResult: imgLink + 'card33-rotate.png',
                    disabled: true
                },
                {
                    id: 3,
                    price: 50,
                    priceOffert: 55,
                    image: imgLink + 'card55.png',
                    imageResult: imgLink + 'card55-rotate.png',
                    disabled: true
                },
                {
                    id: 2,
                    price: 100,
                    priceOffert: 110,
                    image: imgLink + 'card110.png',
                    imageResult: imgLink + 'card110-rotate.png',
                    disabled: true
                },
                {
                    id: 1,
                    price: 200,
                    priceOffert: 220,
                    image: imgLink + 'card220.png',
                    imageResult: imgLink + 'card220-rotate.png',
                    disabled: true
                },


                {
                    id: 5,
                    price: null,
                    image: imgLink + 'card-otros.jpg',
                    imageResult: imgLink + 'card-otros-rotate.png',
                    disabled: false
                }
            ],
        };
    },
    computed: {
        offertDynamic() {
            if (this.isActive.price) {
                return parseFloat(this.isActive.price) + (10 * this.isActive.price) / 100;
            } else {
                return '0';
            }

        },
    },
    methods: {
        myFilter(item) {
            let self = this;
            this.isActive = item;
            this.price = item.price;
            this.imageResult = item.imageResult;
            this.priceOffert = item.priceOffert;
            this.disabled = item.disabled;
            if (item.id == 5) {
                setTimeout(function () {
                    document.getElementById("priceOfertToPay").focus();
                    self.resetPayment();
                }, 500);
            } else {
                if(!this.is_resent_load){
                    this.irAPagar();
                }
            }
            this.is_resent_load = false;

        },
        onlyNumber($event) {
            let keyCode = ($event.keyCode ? $event.keyCode : $event.which);
            // && keyCode !== 46
            if ((keyCode < 48 || keyCode > 57)) {
                $event.preventDefault();
            }
        },
        irAPagar() {
            document.location = '#comprar';
            this.resetPayment();

        },
        resetPayment() {
            if(!this.session_payment){
                this.is_show_continue = true;
                document.getElementById('frmVisaNet').setAttribute("style", "display:none");
                //postscribe('#frmVisaNet', ``);
                document.getElementById('frmVisaNet').innerHTML = '';
            }else{
                alert("Error en la transacción, inténtalo nuevamente.");
                document.location = '/gif-card/error';
            }

        },
        IniciarProcesoPago() {
            if (this.isActive.price >= 5) {
                let self = this;
                this.client.total = this.isActive.price;
                this.is_show_continue = false;
                axios.post('/gif-card/visa/get-session', this.client).then(response => {
                    if (response.data.result) {
                        self.loadVISAButton(response.data);
                    } else {
                        alert("Ha ocurrido un error, inténtano nuevamente");
                        self.is_show_continue = true;
                    }

                }).catch(function (r) {
                    self.is_show_continue = true;
                    alert("Todos los datos son requeridos.");
                });
            } else {
                alert("Monto Inválido");
            }
        },
        loadVISAButton(data) {
            this.action_pay_post = data.action_pay_post + "/" + data.purchase_number;
            this.session_payment = data.session;
            document.getElementById('frmVisaNet').setAttribute("style", "display:auto");
            postscribe('#frmVisaNet', `
                <script type="application/javascript" src="` + data.VISA_URL_JS + `"
                        data-sessiontoken="` + data.session + `"
                        data-channel="web"
                        data-merchantid="` + data.VISA_MERCHANT_ID + `"
                        data-merchantlogo="https://rusticadelivery.com.pe/images/r-delivery-black.png"
                        data-purchasenumber="` + data.purchase_number + `"
                        data-amount="` + this.client.total + `"
                        data-buttonsize="LARGE"
                        data-showamount="TRUE"
                        data-expirationminutes="5"
                        data-timeouturl="https://rusticadelivery.com.pe/"
                >
                </script>
            `);
        }
    },
    created() {
        this.myFilter(this.listCard[2]);
    }
});

