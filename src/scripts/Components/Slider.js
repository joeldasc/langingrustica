import Swiper from 'swiper';

function runSliderOne () {
    var swiper = new Swiper( '.rtica__banner-principal.swiper-container', {
        effect: 'fade',
        loop: true,
        autoplay: {
            delay: 3000,
            disableOnInteraction: false
        }
    });
}

function runSliderTwo () {
    var swiper = new Swiper( '.rtica__banner-detail.swiper-container', {
        loop: true,
        slidesPerView: 1,
        simulateTouch: false,
        pagination: {
            el: '.swiper-pagination',
            clickable: true
        },
        autoplay: {
            delay: 3000,
            disableOnInteraction: false
        },
        breakpoints: {
            768: {
                loop: false,
                slidesPerView: 3
            }
        }
    });
}
setTimeout(runSliderOne, 100);
setTimeout(runSliderTwo, 100);
